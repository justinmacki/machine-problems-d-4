fun main(){
    print("Please input a string: ")
    var userInput = readLine()!!
    println(stringProcessor(userInput))

}

fun stringProcessor(userInput: String): String{
    return if(userInput.length < 16){
        if (userInput.length % 2 == 0) userInput.reversed()
        else userInput.toSortedSet().joinToString("")
    }else "Please use a string with up to 15 characters only."
    return ""
}
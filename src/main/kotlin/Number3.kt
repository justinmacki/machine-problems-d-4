fun main(){
    var exitFlag = false

    var phonebook: MutableMap<String, String> = mutableMapOf(
        "Brandon" to "09773091979",
        "Dan" to "09957842585",
        "Christine" to "09087652938",
        "Kaycee" to "09998240392",
        "Juveil" to "09389402847",
        "Jes" to "0977337379",
        "Glenn" to "09759842585",
        "Kathryn" to "09986437938",
        "CJ" to "09238967392",
        "Ricardo" to "09773217856"
    )

    while(!exitFlag){
        if(phonebook.size <= 30) {
            print("Please enter a name: ")
            var userName = readLine()!!
            if (userName.lowercase() != "quit") checkPhonebook(phonebook, userName)
            else exitFlag = true
        }else println("Phonebook is full!")
    }
}

fun checkPhonebook(phonebook: MutableMap<String,String>, userName: String){
    if (phonebook.containsKey(userName)) println("Number: ${phonebook.getValue(userName)}")
    else {
        print("Please enter a phone number: ")
        var userNumber = readLine()!!
        phonebook[userName] = userNumber
        println("We have added $userName with a number of $userNumber to the phonebook. Thanks!")
    }
}
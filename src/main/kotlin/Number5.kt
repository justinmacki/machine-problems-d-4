var exitFlag = false

fun main(){

    var listOfNames = mutableMapOf("" to "")

    setDetails(listOfNames)
    println()
    listOfNames.remove("")
    println(listOfNames.keys)
    println(listOfNames.size)
    exitFlag = false
    println()

    getDetails(listOfNames)

    println("\nThank you!")
}

fun setDetails(listOfNames: MutableMap<String, String>){
    while (!exitFlag){
        if(listOfNames.size <= 10) {
            print("Please enter a name: ")
            var name = readLine()!!

            if (name != "ZZZ") {
                print("Please enter the birthdate: ")
                var birthdate = readLine()!!
                listOfNames[name] = birthdate
            } else if (name == "ZZZ") exitFlag = true
        }else exitFlag = true
    }
}

fun getDetails(listOfNames: MutableMap<String, String>){
    while (!exitFlag){
        print("Please type a name in the list: ")
        var name = readLine()!!

        if (name != "ZZZ") {
            if (listOfNames.containsKey(name)) println("The birthdate of $name is ${listOfNames.getValue(name)}.")
            else println("Name has not been previously entered to the system.")
        } else if (name == "ZZZ") exitFlag = true
    }
}
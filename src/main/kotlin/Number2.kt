fun main() {
    var colorList = listOf("red", "blue", "yeLlow", "RED", "BluE", "VioLet", "rEd", "pink", "black", "Gray")
    println(checkColors(colorList))
}

fun checkColors(colors: List<Any>): Map<Any, Int> {
    var counter = mutableListOf<Any>()
    val colorsInRainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")

    colors.forEach { colorInput ->
        if (colorsInRainbow.contains(colorInput.toString().lowercase())) {
            counter.add(colorInput.toString().lowercase())
        } else counter.add("others")
    }
    return counter.groupingBy { it }.eachCount()
}
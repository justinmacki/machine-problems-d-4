fun main() {
    val myQuestions = arrayOf(
        "Question" to "What is my favorite food?",
        "Choices" to mapOf(
            "A" to "Adobo",
            "B" to "Sinigang",
            "C" to "Kalamares"
        ),
        "Question" to "What is my go-to restaurant?",
        "Choices" to mapOf(
            "A" to "Mcdonalds",
            "B" to "Jollibee",
            "C" to "Tapsilogan"
        ),
        "Question" to "What is my favorite cuisine?",
        "Choices" to mapOf(
            "A" to "Filipino",
            "B" to "Korean",
            "C" to "Thai"
        ),
        "Question" to "What is my the name of my food buddy?",
        "Choices" to mapOf(
            "A" to "Dan",
            "B" to "Claudine",
            "C" to "Kennedy"
        ),
        "Question" to "What is my favorite recess snack?",
        "Choices" to mapOf(
            "A" to "Turon",
            "B" to "Fishball",
            "C" to "Corndog"
        ),
        "Question" to "What is the name of my favorite university store?",
        "Choices" to mapOf(
            "A" to "Kalvins",
            "B" to "Lynda's",
            "C" to "Prime Plate"
        ),
        "Question" to "What is my preferred taste?",
        "Choices" to mapOf(
            "A" to "Sour",
            "B" to "Salty",
            "C" to "Sweets"
        ),
        "Question" to "What is the taste that I hate the most?",
        "Choices" to mapOf(
            "A" to "Sour",
            "B" to "Salty",
            "C" to "Sweets"
        ),
        "Question" to "Which of the following foods do I hate the most?",
        "Choices" to mapOf(
            "A" to "Laing",
            "B" to "Pinakbet",
            "C" to "Ampalaya"
        ),
        "Question" to "What is your favorite hobby?",
        "Choices" to mapOf(
            "A" to "Volleyball",
            "B" to "Exercise",
            "C" to "Eating"
        ),
    )

    val answer = arrayListOf(
        "A",
        "B",
        "C",
        "A",
        "A",
        "B",
        "C",
        "A",
        "B",
        "C")

    var score = 0
    var ctr = 0
    for(i in answer) {
        println(myQuestions[ctr].second)
        println(myQuestions[ctr+1].second)
        var exitFlag = 0
        while (exitFlag != 1){
            val userInput = readLine()!!
            exitFlag = if (userInput == "A" || userInput == "B" || userInput == "C"){
                if(userInput == i ) {
                    println("Correct!")
                    println()
                    score++
                }
                else println("The correct answer is: $i\n")
                1
            } else 0
        }
        ctr += 2
    }

    println("Your score is: $score")
    println("Your incorrect answer is: ${answer.size-score}")

}